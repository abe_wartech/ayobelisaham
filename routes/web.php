<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('homebtn');
});




Auth::routes();
Route::get('/home_user', 'UserController@index');
Route::get('/login', 'UserController@login');
Route::post('/loginPost', 'UserController@loginPost');
Route::get('/register', 'UserController@register');
Route::post('/registerPost', 'UserController@registerPost');
Route::get('/logout', 'UserController@logout');

Route::get('/homebtn', 'HomebtnController@index')->name('home');
Route::get('/about', 'AboutController@index')->name('about');
Route::get('/begin', 'BeginController@index')->name('begin');

Route::get('/level/', 'LevelController@index')->name('level.index');
Route::delete('/level/{id}', 'LevelController@destroy')->name('level.destroy');
Route::get('/level/{id}', 'LevelController@edit')->name('level.edit');
Route::put('/level/{id}', 'LevelController@update')->name('level.update');


Route::get('admin/home_admin', 'HomebtnController@admin');
Route::get('/admin', 'HomebtnController@admin');



