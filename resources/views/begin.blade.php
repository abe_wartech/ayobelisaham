@extends('single_blade')
@section('section')

<section class="banner-bottom py-5">
		<div class="py-xl-5 py-lg-3">
			<h3 class="tittle text-dark text-center font-weight-bold mb-lg-5 mb-4">Learning</h3>
			<div class="tabs">
				<div class="container">
					<ul class="nav nav-pills my-4" id="pills-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link tabs-w3ls" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home"
							 aria-selected="true">
								<i class="fab fa-500px"></i>
								BAB 1</a>
						</li>
						<li class="nav-item">
							<a class="nav-link tabs-w3ls" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile"
							 aria-selected="false">
								<i class="fab fa-yelp"></i>BAB 2</a>
						</li>
						<li class="nav-item">
							<a class="nav-link tabs-w3ls active" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-profile"
							 aria-selected="false">
								<i class="fab fa-forumbee"></i>BAB 3</a>
						</li>
						<li class="nav-item">
							<a class="nav-link tabs-w3ls" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile"
							 aria-selected="false">
								<i class="fab fa-cloudversify"></i>BAB 4</a>
						</li>
						<li class="nav-item">
							<a class="nav-link tabs-w3ls" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-profile"
							 aria-selected="false">
								<i class="fab fa-audible"></i>BAB 5</a>
						</li>
					</ul>
				</div>
				<div class="tab-content mt-sm-5" id="pills-tabContent">
					<div class="tab-pane fade show active" id="pills-home" role="tabpanel">
						<div class="menu-grids pt-4">
							<div class="d-flex t-in">
								<div class="col-lg-6 text-info-sec p-0">
									<img src="{{asset ('blog/images/w1.png')}}" alt=" " class="img-fluid" />
								</div>
								<div class="col-lg-6 text-info-sec mt-5">
									<div class="service-in">
										<h6>sed do eiusmod</h6>
										<h5 class="what-title text-uppercase my-3">BAB 1</h5>
										<p class="whats-text">Lorem ipsum dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor
											incididunt ut labore.Lorem ipsum
											dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor incididunt ut labore</p>
										<a href="single.html" class="blog-more-agile">Read More</a>
									</div>
								</div>
							</div>
							<div class="d-flex t-in mt-5">
								<div class="col-lg-6 text-info-sec mt-5">
									<div class="service-in-2">
										<h6>sed do eiusmod</h6>
										<h5 class="what-title text-uppercase my-3">BAB 2</h5>
										<p class="whats-text">Lorem ipsum dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor
											incididunt ut labore.Lorem ipsum
											dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor incididunt ut labore</p>
										<a href="single.html" class="blog-more-agile color-w3lsm">Read More</a>
									</div>
								</div>
								<div class="col-lg-6 text-info-sec p-0">
									<img src="{{asset ('blog/images/w2.png')}}" alt=" " class="img-fluid" />
								</div>
							</div>
							<div class="d-flex t-in">
								<div class="col-lg-6 text-info-sec p-0">
									<img src="{{asset ('blog/images/w1.png')}}" alt=" " class="img-fluid" />
								</div>
								<div class="col-lg-6 text-info-sec mt-5">
									<div class="service-in">
										<h6>sed do eiusmod</h6>
										<h5 class="what-title text-uppercase my-3">BAB 3</h5>
										<p class="whats-text">Lorem ipsum dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor
											incididunt ut labore.Lorem ipsum
											dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor incididunt ut labore</p>
										<a href="single.html" class="blog-more-agile">Read More</a>
									</div>
								</div>
							</div>
							<div class="d-flex t-in mt-5">
								<div class="col-lg-6 text-info-sec mt-5">
									<div class="service-in-2">
										<h6>sed do eiusmod</h6>
										<h5 class="what-title text-uppercase my-3">BAB 4</h5>
										<p class="whats-text">Lorem ipsum dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor
											incididunt ut labore.Lorem ipsum
											dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor incididunt ut labore</p>
										<a href="single.html" class="blog-more-agile color-w3lsm">Read More</a>
									</div>
								</div>
								<div class="col-lg-6 text-info-sec p-0">
									<img src="{{asset ('blog/images/w2.png')}}" alt=" " class="img-fluid" />
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="pills-profile" role="tabpanel">
						<div class="menu-grids pt-4">
							<div class="d-flex t-in">
								<div class="col-lg-6 text-info-sec mt-5">
									<div class="service-in-2">
										<h6>sed do eiusmod</h6>
										<h5 class="what-title text-uppercase my-3">Heading Here</h5>
										<p class="whats-text">Lorem ipsum dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor
											incididunt ut labore.Lorem ipsum
											dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor incididunt ut labore</p>
										<a href="single.html" class="blog-more-agile color-w3lsm">Read More</a>
									</div>
								</div>
								<div class="col-lg-6 text-info-sec p-0">
									<img src="{{asset ('blog/images/w2.png')}}" alt=" " class="img-fluid" />
								</div>
							</div>
							<div class="d-flex t-in mt-5">
								<div class="col-lg-6 text-info-sec p-0">
									<img src="{{asset ('blog/images/w1.png')}}" alt=" " class="img-fluid" />
								</div>
								<div class="col-lg-6 text-info-sec mt-5">
									<div class="service-in">
										<h6>sed do eiusmod</h6>
										<h5 class="what-title text-uppercase my-3">Heading Here</h5>
										<p class="whats-text">Lorem ipsum dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor
											incididunt ut labore.Lorem ipsum
											dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor incididunt ut labore</p>
										<a href="single.html" class="blog-more-agile">Read More</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


@endsection