


<header class="main-header">
    <!-- Logo -->
    <a href="{{url ('/') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">
        <img src="{{asset('AdminLTE/images/')}}" height="45px">
      </span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg" style="background: white;border-bottom-color: black;">
        <img src="{{asset('AdminLTE/images/')}}" height="45px">
      </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        
      </div>
    </nav>
  </header>