<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li><a href="{{url ('/home_admin') }}"><i class="fa fa-home"></i> <span>Home</span></a></li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li><a href="{{url ('../home_admin') }}"><i class="fa fa-cubes"></i> <span>Level</span></a></li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li><a href="{{url ('materi/level') }}"><i class="fa fa-book"></i> <span>Materi</span></a></li>
      </ul>

    </section>
    <!-- /.sidebar -->
  </aside>