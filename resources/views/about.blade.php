@extends('main_layout')

@section('content')
	<!-- about section -->
	<div class="about-w3ls-inner py-5" id="gallery">
		<div class="container py-xl-5 py-lg-3">
			<h3 class="tittle text-dark text-center font-weight-bold mb-lg-5 mb-4">Apa Itu Saham?</h3>
			<div class="row">
				<div class="col-lg-6 inner-w3ls-about">
					<h3 class="about-style font-weight-light font-italic my-3">Saham can make your life be
						<span class="font-weight-bold mt-2">Better</span>
					</h3>
					<p class="inner-para-w3l text-right my-4 pr-4 border-right">saham saham saham saham saham saham saham saham saham
                    saham saham saham saham saham</p>
				</div>
				<div class="col-lg-6 inner-w3ls-about-right">
					<img src="{{asset ('blog/images/d1.PNG')}}" alt="" class="img-fluid">
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-lg-6 inner-w3ls-about-right">
					<img src="{{asset ('blog/images/d2.PNG')}}" alt="" class="img-fluid">
				</div>
				<div class="col-lg-6 inner-w3ls-about">
					<div class="about-bottom mb-5">
						<ul> 
							<li>
								<h5>2000+</h5>
								<p>Sit Amet</p>
							</li>
							<li>
								<h5>3600+</h5>
								<p>It Labore</p>
							</li>
							<li>
								<h5>1200+</h5>
								<p>Dole Mag</p>
							</li>
						</ul>
					</div>
					<p class="inner-para-w3l-2 text-left my-4 pl-4 border-left">Sed ut perspiciatis unde omnis iste natus error sit
						voluptatem accusantium doloremque laudantium, totam rem aperiam,
						eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>

				</div>
			</div>
		</div>
	</div>
	<!-- //about section -->

	<div class="tab-pane fade" id="pills-profile" role="tabpanel">
						<div class="menu-grids pt-4">
							<div class="d-flex t-in">
								<div class="col-lg-6 text-info-sec mt-5">
									<div class="service-in-2">
										<h6>sed do eiusmod</h6>
										<h5 class="what-title text-uppercase my-3">Heading Here</h5>
										<p class="whats-text">Lorem ipsum dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor
											incididunt ut labore.Lorem ipsum
											dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor incididunt ut labore</p>
										<a href="single.html" class="blog-more-agile color-w3lsm">Read More</a>
									</div>
								</div>
								<div class="col-lg-6 text-info-sec p-0">
									<img src="{{asset ('blog/images/w2.png')}}" alt=" " class="img-fluid" />
								</div>
							</div>
							<div class="d-flex t-in mt-5">
								<div class="col-lg-6 text-info-sec p-0">
									<img src="{{asset ('blog/images/w1.tabitha-guest-color-1280x640.jpg')}}" alt=" " class="img-fluid" />
								</div>
								<div class="col-lg-6 text-info-sec mt-5">
									<div class="service-in">
										<h6>sed do eiusmod</h6>
										<h5 class="what-title text-uppercase my-3">Heading Here</h5>
										<p class="whats-text">Lorem ipsum dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor
											incididunt ut labore.Lorem ipsum
											dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor incididunt ut labore</p>
										<a href="single.html" class="blog-more-agile">Read More</a>
									</div>
								</div>
							</div>
						</div>
					</div>

	<!-- stats-->
	<<div class="tab-content mt-sm-5" id="pills-tabContent">
					<div class="tab-pane fade show active" id="pills-home" role="tabpanel">
						<div class="menu-grids pt-4">
							<div class="d-flex t-in">
								<div class="col-lg-6 text-info-sec p-0">
									<img src="{{asset ('blog/images/tabitha-guest-color-1280x640.jpg')}}" alt=" " class="img-fluid" />
								</div>
								<div class="col-lg-6 text-info-sec mt-5">
									<div class="service-in">
										<h6>sed do eiusmod</h6>
										<h5 class="what-title text-uppercase my-3">BEGINNER</h5>
										<p class="whats-text">Lorem ipsum dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor
											incididunt ut labore.Lorem ipsum
											dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor incididunt ut labore</p>
										<a href="begin" class="blog-more-agile">Read More</a>
									</div>
								</div>
							</div>
							<div class="d-flex t-in mt-5">
								<div class="col-lg-6 text-info-sec mt-5">
									<div class="service-in-2">
										<h6>sed do eiusmod</h6>
										<h5 class="what-title text-uppercase my-3">ADVANCE</h5>
										<p class="whats-text">Lorem ipsum dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor
											incididunt ut labore.Lorem ipsum
											dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor incididunt ut labore</p>
										<a href="single.html" class="blog-more-agile color-w3lsm">Read More</a>
									</div>
								</div>
								<div class="col-lg-6 text-info-sec p-0">
									<img src="{{asset ('blog/images/w2.png')}}" alt=" " class="img-fluid" />
								</div>
							</div>
						</div>
					</div>
					
	<!-- //stats -->

	<!-- trainers -->
	<section class="bottom-banner-w3layouts py-5">
		<div class="container py-xl-5 py-lg-3">
			<h3 class="tittle text-dark text-center font-weight-bold mb-lg-5 mb-4">Our Trainers</h3>
			<div class="row inner-sec-w3ls-agileinfo ">
				<div class="col-md-4">
					<div class="team-grid text-center">
						<div class="team-img">
							<img class="img-fluid rounded" src="{{asset ('blog/images/team1.jpg')}}" alt="">
						</div>
						<div class="team-info">
							<h4>Expert 1</h4>
							<span>Chief Executive </span>
							<ul class="d-flex justify-content-center py-3 social-icons">
								<li>
									<a href="#">
										<i class="fab fa-facebook-f"></i>
									</a>
								</li>
								<li class="mx-3">
									<a href="#">
										<i class="fab fa-twitter"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fab fa-google-plus-g"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="team-grid text-center">
						<div class="team-img">
							<img class="img-fluid rounded" src="{{asset ('blog/images/team2.jpg')}}" alt="">
						</div>
						<div class="team-info">
							<h4>Expert 2</h4>
							<span>Senior Engineer</span>
							<ul class="d-flex justify-content-center py-3 social-icons">
								<li>
									<a href="#">
										<i class="fab fa-facebook-f"></i>
									</a>
								</li>
								<li class="mx-3">
									<a href="#">
										<i class="fab fa-twitter"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fab fa-google-plus-g"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="team-grid text-center">
						<div class="team-img">
							<img class="img-fluid rounded" src="{{asset ('blog/images/team3.jpg')}}" alt="">
						</div>
						<div class="team-info">
							<h4>Expert 3</h4>
							<span>HR Manager</span>
							<ul class="d-flex justify-content-center py-3 social-icons">
								<li>
									<a href="#">
										<i class="fab fa-facebook-f"></i>
									</a>
								</li>
								<li class="mx-3">
									<a href="#">
										<i class="fab fa-twitter"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fab fa-google-plus-g"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- //trainers -->
@endsection


