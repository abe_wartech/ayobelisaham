@extends('main_layout')
@section('content')



<div class="main-sec" id="home">
		<!-- header -->
		<header>
			<div class="wrap">
				<nav class="navbar navbar-expand-lg navbar-light shift">
					<div class="logo">
						<h1>
							<a class="navbar-brand" href="index.html">
								Skater
								<img src="{{URL::to('/')}}/blog/images/logo3.png" alt="">
							</a>
						</h1>
					</div>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mx-auto">
							<li class="nav-item">
								<a class="nav-link active" href="home.html">Home
									<span class="sr-only">(current)</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="about.html">About Us</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="register">Register</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="login">Login</a>
							</li>

							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
								 aria-haspopup="true" aria-expanded="false">
									Pages
									<i class="fas fa-angle-down"></i>
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown">
									<a class="dropdown-item" href="services.html">Services</a>
									<a class="dropdown-item" href="single.html">Some More</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="404.html">Error Page</a>
								</div>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="gallery.html">Gallery</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="contact.html">Contact Us</a>
							</li>
						</ul>
						<!-- Social Icons -->
						<div class="social_media">
							<ul>
								<li>
									<a href="#" class="facebook">
										<i class="fab fa-facebook-f" aria-hidden="true"></i>
									</a>
								</li>
								<li class="mx-2">
									<a href="#" class="twitter">
										<i class="fab fa-twitter" aria-hidden="true"></i>
									</a>
								</li>
								<li>
									<a href="#" class="dribbble">
										<i class="fab fa-dribbble" aria-hidden="true"></i>
									</a>
								</li>
							</ul>
						</div>
						<!-- //Social Icons -->
					</div>
				</nav>
			</div>
		</header>
		<!--//header-->

		<!-- banner-miain -->
		<div class="page-head_agile_info_w3l-2">
			<!-- cup animation -->
			<div class="circle">
			</div>
			<!-- //cup animation -->
		</div>
		<!-- //banner-miain -->
	</div>
	<!-- banner -->

	<!-- pages -->
	<div class="w3ls-main py-5">
		<div class="container py-xl-5 py-lg-3">
			<div class="title text-center mb-sm-5 mb-4">
				<h3 class="tittle text-dark text-center font-weight-bold mb-lg-5 mb-4">Choose a Demo</h3>
			</div>
			<div class="row next-section pt-4">
				<div class="col-md-4 filtr-item">
					<div class="grids-w3l">
						<div class="style-grids">
							<a href="homebtn" target="_blank" title="Skater">
								<img src="{{asset ('blog/images/p1.jpg')}}" alt="" class="img-fluid" />
							</a>
						</div>
					</div>
					<div class="title">
						<h6 class="text-dark text-center mt-4">Index Page</h6>
					</div>
				</div>
				<div class="col-md-4 filtr-item my-md-0 my-4">
					<div class="grids-w3l">
						<div class="style-grids">
							<a href="about.html" target="_blank" title="Skater">
								<img src="{{asset ('blog/images/p2.jpg')}}" alt="" class="img-fluid" />
							</a>
						</div>
					</div>
					<div class="title">
						<h6 class="text-dark text-center mt-4">About Page</h6>
					</div>
				</div>
				<div class="col-md-4 filtr-item">
					<div class="grids-w3l">
						<div class="style-grids">
							<a href="gallery.html" target="_blank" title="Skater">
								<img src="{{asset ('blog/images/p3.jpg')}}" alt="" class="img-fluid" />
							</a>
						</div>
					</div>
					<div class="title">
						<h6 class="text-dark text-center mt-4">Gallery Page</h6>
					</div>
				</div>
			</div>
			<div class="row next-section my-md-5 pt-4">
				<div class="col-md-4 filtr-item">
					<div class="grids-w3l">
						<div class="style-grids">
							<a href="single.html" target="_blank" title="Skater">
								<img src="{{asset ('blog/images/p4.jpg')}}" alt="" class="img-fluid" />
							</a>
						</div>
					</div>
					<div class="title">
						<h6 class="text-dark text-center mt-4">Single Page</h6>
					</div>
				</div>
				<div class="col-md-4 filtr-item  my-md-0 my-4">
					<div class="grids-w3l">
						<div class="style-grids">
							<a href="404.html" target="_blank" title="Skater">
								<img src="{{asset ('blog/images/p5.jpg')}}" alt="" class="img-fluid" />
							</a>
						</div>
					</div>
					<div class="title">
						<h6 class="text-dark text-center mt-4">404 Error Page</h6>
					</div>
				</div>
				<div class="col-md-4 filtr-item">
					<div class="grids-w3l">
						<div class="style-grids">
							<a href="contact.html" target="_blank" title="Skater">
								<img src="{{asset ('blog/images/p6.jpg')}}" alt="" class="img-fluid" />
							</a>
						</div>
					</div>
					<div class="title">
						<h6 class="text-dark text-center mt-4">Contact Page</h6>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //pages -->
@endsection