<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelLevel extends Model
{
    protected $table = 'level';
}
