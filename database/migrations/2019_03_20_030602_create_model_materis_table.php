<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelMaterisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('materi', function (Blueprint $table) {
            $table->increments('id'); //membuat kolom id auto increment
             $table->string('level_id'); 
            $table->string('judul_materi'); //membuat kolom judul bab
            $table->string('content_materi'); //membuat kolom content materi
            $table->timestamps(); //membuat kolom created_at dan updated_at sebagai fungsi dasar laravel
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_materis');
    }
}
