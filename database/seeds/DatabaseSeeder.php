<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LevelSeeder::class); //memanggil class LevelSeeder
        $this->call(MateriSeeder::class); //memanggil class MateriSeeder
    }


}
