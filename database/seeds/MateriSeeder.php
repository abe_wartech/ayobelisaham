<?php

use Illuminate\Database\Seeder;
use App\Service;


class MateriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
        //
        $faker = Faker\Factory::create(); //import library faker

        $limit = 5; //batasan berapa banyak data

        for ($i = 0; $i < $limit; $i++) {
            DB::table('materi')->insert([ //mengisi data di database
                'level_id'->$faker->id,
                'judul_materi'->$faker->judul_materi,
                'content_materi'->$faker->content_materi,
            ]);
        }
    }
}
